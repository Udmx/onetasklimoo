from flask import request
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_required, get_jwt_identity
from sqlalchemy.exc import IntegrityError
from config import app
from config.utils.request import json_only
from config.models import User
from config import db


@app.route('/auth/register', methods=['POST'])
@json_only
def create_user():
    args = request.get_json()

    try:
        new_user = User()
        new_user.username = args.get('username')
        new_user.password = args.get('password')
        db.session.add(new_user)
        db.session.commit()
    except ValueError as e:
        db.session.rollback()
        return {'error': f'{e}'}, 400
    except IntegrityError:
        db.session.rollback()
        return {'error': 'Username is duplicated.'}, 400
    return {'message': 'Account created successfully'}, 201


@app.route('/auth/login', methods=['POST'])
@json_only
def login():
    if not request.is_json:
        return {'error': 'JSON Only!'}, 400

    args = request.get_json()
    username = args.get('username')
    password = args.get('password')

    user = User.query.filter(User.username.ilike(username)).first()
    if not user:
        return {'error': 'Username or Password does not match'}, 403

    if not user.check_password(password):
        return {'error': 'Username or Password does not match'}, 403

    access_token = create_access_token(identity=user.username, fresh=True)
    refresh_token = create_refresh_token(identity=user.username)
    return {'access_token': access_token, 'refresh_token': refresh_token}, 200


@app.route('/auth/login', methods=['PUT'])
@jwt_required()
def get_new_access_token():
    identity = get_jwt_identity()
    return {'access_token': create_access_token(identity=identity)}


@app.route('/accounts/<username>', methods=['GET'])
def get_profile(username):
    user = User.query.filter(User.username.ilike(username)).first()
    if user is None:
        return {'error': 'Username is not found.'}, 404
    return {'username': user.username}


@app.route('/auth/modify', methods=['PATCH'])
@jwt_required()
@json_only
def modify_profile():
    args = request.get_json()

    identity = get_jwt_identity()
    user = User.query.filter(User.username.ilike(identity)).first()

    new_password = args.get('password')
    try:
        user.password = new_password
        db.session.commit()
    except ValueError as e:
        db.session.rollback()
        return {'error': f'{e}'}, 400
    return {}, 204


@app.route('/auth/delete', methods=['DELETE'])
@jwt_required()
def delete_profile():
    identity = get_jwt_identity()
    user = User.query.filter(User.username.ilike(identity)).first()

    try:
        db.session.delete(user)
        db.session.commit()
    except ValueError as e:
        db.session.rollback()
        return {'error': f'{e}'}, 400
    return {}, 204
