from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../limoo.db'
app.config["JWT_SECRET_KEY"] = "i love limoo"

db = SQLAlchemy(app)
jwt_manager = JWTManager(app)

from config import routes
